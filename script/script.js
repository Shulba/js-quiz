let quizDate = {
	id: 0,
	positiveAnswer: 0,
	nagativeAnswer: 0,
	points:0,
	totalLevel: 0
}

let dateModel = {
	appModel(method, url ){
		let mainDate = new XMLHttpRequest();
		mainDate.open("GET", url);
		mainDate.onload = () => {
			let ourDate = JSON.parse(mainDate.responseText);
			quizDate.totalLevel = ourDate.length;
			controllerModule.displayDataModule.call(ourDate)
		};
		mainDate.onerror =()=>{
			console.log(`An Error has been occured`)	// відстеження помилок
		};
		mainDate.send()
	}
}
function ClosureFunction(){
		let closere =  function(url){
			return url
		}
		return closere
	}
let controllerModule = {
	displayDataModule(){
		visualModule.render(this[quizDate.id]);
		let object = new ClosureFunction();
		controllerModule.quizObject = object(this[quizDate.id]);
	},
	toJSONString(){
		let obj = {};
		let elements = document.querySelectorAll('input');
		this.allQustionElments = elements;
		for( let i = 0; i < elements.length; ++i ) {
			let element = elements[i];
			let name = element.name;
			let value;
			if(controllerModule.quizObject.seletorType === "checkbox"){
				value = element.checked;
				this.pushToObject(name, obj, value)
			}else{
				value = element.value;
				this.pushToObject(name, obj, value)
				if(element.checked){
					break
				}
			}
		}
		let res = this.answerCheck(obj)
		return [JSON.stringify( obj ), res];
	},
	pushToObject(objName, object, val){
			if( objName ) {
				object[ objName ] = val;
			}
	},
	answerCheck(obj){
		let an = this.quizObject.corectAnswer;
		let locAn = [];
		let match = false
		for(i in obj){
			if(obj[i]){
				if(this.quizObject.seletorType === "checkbox"){
					locAn.push(i)
				}else{
					locAn.push(obj[i])
				}
			}
		}
		if(locAn.length === an.length){
			an.forEach((x, i)=>{
				if(x == locAn[i]){
					match = true
				}else{
					match = false
				}
			})
		}else{
			match = false
		}
		return match
	},
	startPoint(obj){
		document.addEventListener("DOMContentLoaded", function(){
			visualModule.resultBox = document.getElementById('displayResults');
			controllerModule.makeCall();
			visualModule.formObject = document.getElementsByName('myForm')[0];
			visualModule.output = document.getElementsByName("output" )[0];
			visualModule.formObject.addEventListener( "submit", function( e ) {
				e.preventDefault();			// stop reload function
				let json = obj.toJSONString()[0];
				visualModule.displayInformation(json, obj.toJSONString()[1])
			}, false)
		})
	},
	makeCall(){
		let URL = "http://localhost:5020/dataset";
		dateModel.appModel("GET", URL);
	}
}
let visualModule = {
	resultBox: '',
	render(objects){
		let qu = `<p>${objects.question}</p>`
		let an = (objects.seletorType === "checkbox")
		? this.generateQuestion(objects.answersNumbers, "checkbox")
		: this.generateQuestion(objects.answersNumbers, "radio")

		let finalQuizBox = `${qu} ${an}`
		this.formObject.innerHTML = finalQuizBox;
		this.resultBlock = document.getElementsByName('resultField')[0];
	},
	generateQuestion(obj, attribute){
		let anField = ''
		let objectName;
		if(obj){
			for(i in obj){
				(attribute == "checkbox")? objectName = i : objectName = "obj";
				anField += `<input name=${objectName} value=${i} type=${attribute}> <label>${obj[i]}</label><br>`
			}
			anField +=`<input type="submit" value="result"><label name="resultField"></label>`
		}else{
			anField = 'End of quiz'
		}
		this.showResults(true)
		return anField
	},
	displayInformation(obj, quizResult){
		let text = `<input type="button" name="next" value="${
			(quizDate.id+1!=quizDate.totalLevel)
			? 'Next Quiz': 'Show Result'
		}" >`
		if(quizResult){
			quizDate.positiveAnswer += 1;
			this.resultBlock.innerHTML = `<span class='success'>Passed</span> ${text}`;
		}else{
			quizDate.nagativeAnswer += 1;
			this.resultBlock.innerHTML = `<span class='reject'>Failed</span> ${text}`;
		}
		controllerModule.allQustionElments.forEach((x) => {
			x.setAttribute("disabled", "")
		})
		this.output.innerHTML = obj;

		document.getElementsByName('next')[0].addEventListener('click', function(){
			(quizResult)? quizDate.points += controllerModule.quizObject.points : quizDate.points;
			if((quizDate.id+1!=quizDate.totalLevel)){
				quizDate.id+=1;
				controllerModule.makeCall();
			}else{
				visualModule.showResults(false)
			}
		})
	},
	showResults(state){
		let text = '';
		if(state){
			text = `<p>${quizDate.id+1} of ${quizDate.totalLevel} question</p>`
		}else if(state == false){
			text = `<p>${quizDate.id+1} of ${quizDate.totalLevel} question</p>
			<p>Total Score: ${quizDate.points} 
			<br>Correct answer: <span class="success"> ${quizDate.positiveAnswer}</span>
			<br>Wrong answer:<span class="reject">${quizDate.nagativeAnswer}</span>
			</p>`
			document.getElementsByName('next')[0].remove()
		}
		this.resultBox.innerHTML = text;
	}
}

controllerModule.startPoint(controllerModule)