const http = require('http'); 
const express =  require('express');
const cors = require('cors');
var fs = require('fs');
var app = express();

app.use(cors())

app.get('/dataset', function(req, res, next){
    let obj;
    fs.readFile('../api/grade/dataset.json', 'utf8', function(err, data) {
        obj = JSON.parse(data);
        res.send(obj);
    })
})

app.listen(5020, function(){
    console.log('Cors')
})
